# -*- coding: utf-8 -*-
"""
Created on Sat Dec 12 09:58:37 2020

@author: Rajat
"""

import numpy as np
import matplotlib.pyplot as plt 

np.random.seed(42)
n=10
p=0.7

# let us repeat our experiment for 10000 times
size=10000
x = np.random.binomial(n=n, p=p, size=size)
print(x)

plt.hist(x)
plt.show()

b = 10
a = 0.3
size=10000
y = np.random.binomial(n=b, p=a, size=size)
print(y)

plt.hist(y)
plt.show()